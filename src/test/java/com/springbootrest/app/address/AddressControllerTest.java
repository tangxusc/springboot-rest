package com.springbootrest.app.address;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(AddressController.class)
public class AddressControllerTest {

    @Autowired
    private MockMvc mvc;
    
    @MockBean
	private AddressRepository addressRepository;

    @Test
    public void createAnValidAddressShouldSuccess()
      throws Exception {
    	Address givenAddress = new Address();
    	givenAddress.setCity("CityName");
    	givenAddress.setState("SC");
    	
    	when(addressRepository.save(givenAddress)).thenReturn(new Address(1L, "CityName", "SC"));
    	
    	String json = (new ObjectMapper()).writeValueAsString(givenAddress);
    	this.mvc.perform(post("/v1/addresses").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isOk());
    }
    
    @Test
    public void createAnExistingAddressShouldConflict()
      throws Exception {
    	Address givenAddress = new Address();
    	givenAddress.setCity("CityName");
    	givenAddress.setState("SC");
    	
    	when(addressRepository.findAddressByCityAndStateIgnoreCase(givenAddress.getCity(), givenAddress.getState())).thenReturn(Optional.of(givenAddress));
    	
    	String json = (new ObjectMapper()).writeValueAsString(givenAddress);
    	this.mvc.perform(post("/v1/addresses").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isConflict());
    }
    
    @Test
    public void createANotValidAddressStateShouldReturnBadRequest()
      throws Exception {
    	Address givenAddress = new Address();
    	givenAddress.setCity("City");
    	givenAddress.setState("SCC");
    	
    	String json = (new ObjectMapper()).writeValueAsString(givenAddress);
    	this.mvc.perform(post("/v1/addresses").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isBadRequest());
    }
    
    @Test
    public void createANotValidAddressCityShouldReturnBadRequest()
      throws Exception {
    	Address givenAddress = new Address();
    	givenAddress.setCity("City that has more than 30 characters");
    	givenAddress.setState("SC");
    	
    	String json = (new ObjectMapper()).writeValueAsString(givenAddress);
    	this.mvc.perform(post("/v1/addresses").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
	    	.andDo(print())
	    	.andExpect(status().isBadRequest());
    }
}