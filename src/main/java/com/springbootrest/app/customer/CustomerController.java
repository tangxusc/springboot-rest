package com.springbootrest.app.customer;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springbootrest.app.NotFoundException;
import com.springbootrest.app.address.Address;
import com.springbootrest.app.address.AddressRepository;

@RestController
@RequestMapping("v1")
public class CustomerController {
	
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private AddressRepository addressRepository;

	@PostMapping("/customers")
	public Customer createCustomer(@Valid @RequestBody Customer customer) {
		Optional<Address> address = addressRepository.findById(customer.getAddressId());
		
		if(!address.isPresent()) {
			throw new NotFoundException("Address Not Found -" + customer.getAddressId());
		}
		
		customer.setAddress(address.get());
		Customer savedCustomer = customerRepository.save(customer);

		return savedCustomer;
	}
	
	@GetMapping("customers")
	public List<Customer> getCustomers(@RequestParam(required = false) String name) {
		List<Customer> customer = customerRepository.findCustomerByNameIgnoreCase(name);
		return customer;
	}
	
	@GetMapping("/customers/{id}")
	public Customer retrieveCustomer(@PathVariable long id) {
		Optional<Customer> customer = customerRepository.findById(id);

		if (!customer.isPresent())
			throw new NotFoundException("id-" + id);

		return customer.get();
	}
	
	@PatchMapping("/customers/{id}")
	public Customer partialUpdateName(@PathVariable long id, @RequestBody String name) {
		Optional<Customer> customer = customerRepository.findById(id);

		if (!customer.isPresent())
			throw new NotFoundException("id-" + id);
		
		Customer updateCustomer = customer.get();
		updateCustomer.setName(name);
		return customerRepository.save(updateCustomer);
	}
	
	@DeleteMapping("/customers/{id}")
	public void deleteCustomer(@PathVariable long id) {
		customerRepository.deleteById(id);
	}
}