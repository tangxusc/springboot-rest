package com.springbootrest.app.customer;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.springbootrest.app.address.Address;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Customer {
	@Id
	@GeneratedValue
	@ApiModelProperty(hidden= true)
	private Long id;
	@NotNull
    @Size(max=100, message="City should maximum of 100 characters")
	private String name;
	@NotNull
    @Size(min=1, max=1, message="Gender should be M for male and F for Female")
	private String gender;
	private Date birthDate;
	@ManyToOne
	@ApiModelProperty(hidden= true)
	private Address address;
	@Transient
	private long addressId;
	
	public Customer() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public long getAddressId() {
		return addressId;
	}
	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}
}

