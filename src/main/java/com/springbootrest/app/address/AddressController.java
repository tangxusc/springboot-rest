package com.springbootrest.app.address;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springbootrest.app.ConflictException;
import com.springbootrest.app.NotFoundException;

@RestController()
@RequestMapping("v1/")
public class AddressController {
	@Autowired
	private AddressRepository addressRepository;

	@PostMapping("addresses")
	public Address Address(@Valid @RequestBody Address address) {
		Optional<Address> existingAddress = 
				addressRepository.findAddressByCityAndStateIgnoreCase(address.getCity(), address.getState());
		
		if(existingAddress.isPresent())
			throw new ConflictException("address already exixts");
		
		Address savedAddress = addressRepository.save(address);

		return savedAddress;
	}
	
	@GetMapping("addresses")
	public List<Address> getAddresses(@RequestParam(required = false) String city, 
			@RequestParam(required = false) String state) {
		List<Address> address = addressRepository.findAddressByCityOrStateIgnoreCase(city, state);
		return address;
	}
	
	@GetMapping("addresses/{id}")
	public Address retrieveAddress(@PathVariable long id) {
		Optional<Address> address = addressRepository.findById(id);

		if (!address.isPresent())
			throw new NotFoundException("id-" + id);

		return address.get();
	}
}
